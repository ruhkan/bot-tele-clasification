import os
import telegram
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackContext
import tensorflow as tf
import numpy as np
from PIL import Image
from io import BytesIO

TOKEN = 'token telegram'  
MODEL_PATH = 'models/keras_model.h5'


class TeethClassifierBot:
    def __init__(self, token):
        print("Bot is starting...")
        self.bot = telegram.Bot(token=token)
        self.updater = Updater(token=token, use_context=True)
        self.dp = self.updater.dispatcher

        # Load the model during initialization
        self.model = tf.keras.models.load_model(MODEL_PATH)

        # Handlers
        self.dp.add_handler(CommandHandler("start", self.start_command))
        self.dp.add_handler(MessageHandler(Filters.photo, self.handle_message))
        self.dp.add_error_handler(self.error_callback)

    def classify_image(self, image):
        image = image.resize((224, 224))
        image = np.array(image) / 255.0
        image = np.expand_dims(image, axis=0)
        predictions = self.model.predict(image)
        return "Gigi Lubang" if predictions[0][0] > predictions[0][1] else "Gigi Sehat"

    def handle_message(self, update: telegram.Update, context: CallbackContext):
        user_id = update.message.from_user.id
        file_id = update.message.photo[-1].file_id
        file = self.bot.get_file(file_id)
        image_file = BytesIO(file.download_as_bytearray())
        image = Image.open(image_file)

        result = self.classify_image(image)
        self.bot.send_message(chat_id=user_id, text=f"Hasil klasifikasi: {result}")

    def error_callback(self, update: telegram.Update, context: CallbackContext):
        print(f"Error occurred: {context.error}")
        try:
            self.bot.send_message(chat_id=update.message.chat_id, text="Maaf, terjadi kesalahan dalam pemrosesan pesan Anda.")
        except Exception as e:
            print(f"Failed to send error message due to: {e}")

    def start_command(self, update: telegram.Update, context: CallbackContext):
        user_id = update.message.from_user.id
        self.bot.send_message(chat_id=user_id, text="Bot telah dimulai. Kirim foto untuk mengklasifikasikan gigi.")

    def run(self):
        self.updater.start_polling()
        self.updater.idle()


if __name__ == "__main__":
    bot = TeethClassifierBot(TOKEN)
    bot.run()
